FROM molgenis/maven-jdk15:latest
WORKDIR /app
COPY . .
RUN mvn package
CMD java -jar target/svod-0.0.1-SNAPSHOT.jar
